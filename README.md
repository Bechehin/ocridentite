# Application Android 

Il y a 3 briques technologiques dans cette application
* Reconnaissance de caractères (ocrClasses)
* Connection au cloud (cloud)
* Certification de Documents

## TODO
* Refactoring FileManagement et DocumentList (afficher les bons types de documents)
* Rendre l'application plus jolie
* Plus de commentaires

## Packaging

###Certfification de document

* hash de l'image et signature (genkey with RSA and Hash with SHA-256)
* Sauvegarde de la signature dans file 
* (TODO send the file) et voir comment send public key

### cloud : tous ce qui est en rapport avec l'envoie de documents dur le cloud

* Connection au cloud
* Upload d'Image
* Creation de folder et parcours dans les folders


### ocrClasses

* Gestion des Files (TODO need refactoring)
* Paramètre de l'application 
* ScanInformation et Scan contient toutes les informations relative au documents
* My Camera gestion de la camera
* CustomDocumentList pour la vue Android

### typedoc

* DocumentType interface pour les differents documents a scanner
* Pour le moment Carte d'Identité et Passeport contient l'algorithme concernant MRZ ( machine readable zone) et l'algorithme de contrôle
* If you want to Add a typedoc Extends DocumentType and add in ImageButtonAdaptater your picture and create an other Information preview and change
saveInformationAsText in FileManagement and create something to do in MainActivity (listener)

### Les Activity 

* SplashScreen (copy les fichier nécessaire à l'OCR et initialise tesseract)
* MainActivity (affiche les différents type de document que l'on peut scanner)
* DocumentList (affiche les documents déjà sauvegardé)
* SettingsActivity (paramètre de l'application)
* CameraPreview (camera affichage)
* Image_Preview (affiche l'image prise par la camera)
* Inforamtion_Preview (affcihe les informations resortis grâce à l'algorithme)

## Parcours utilisateur

* MainActivity (choix du documents à scanner)
* DocumentsList (visualisation des photos déjà prise pour ce type de documents)
* CameraPreview (active la camera)
* Image_Preview 
* Information_Preview

## Test
* Test de la certification et des algorithme de caractere de controle 
* In test caractère de controle need to change the String (RMZ fausse donc les tests ne passent pas)