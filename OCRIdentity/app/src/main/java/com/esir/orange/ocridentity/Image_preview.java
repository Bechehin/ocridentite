package com.esir.orange.ocridentity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.esir.orange.ocridentity.ocrClasses.FilesManagement;
import com.esir.orange.ocridentity.typedoc.DocumentType;


/*
* Created by Oracion
*/

public class Image_preview extends Activity {

    public static Bitmap bitmap = null;
    FilesManagement mgm = new FilesManagement();
    DocumentType s_type_doc;
    private String path = "";
    private String TAG = "ImagePreview";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);

        s_type_doc = (DocumentType) getIntent().getSerializableExtra("s_type_doc");

        if (bitmap != null) {

            ImageView view = (ImageView) findViewById(R.id.preview_after_scan);
            ImageButton btn_save = (ImageButton) findViewById(R.id.btn_save_after_scan);
            ImageButton btn_delete = (ImageButton) findViewById(R.id.btn_del_after_scan);
            view.setImageBitmap(bitmap);

            if (Information_preview.capture == 0) {
                btn_delete.setVisibility(View.INVISIBLE);
                btn_save.setVisibility(View.INVISIBLE);
            } else {
                btn_delete.setVisibility(View.VISIBLE);
                btn_save.setVisibility(View.VISIBLE);
            }

            btn_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cancel(false);
                }
            });

            btn_save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mgm.savePhoto(getApplicationContext(), bitmap, DocumentsList.currentPath)) {
                        if (!s_type_doc.getType().equals("default")) {
                            mgm.extractInformation(s_type_doc);
                        }
                        Information_preview.capture = 1;
                        cancel(true);

                    } else {
                        Toast.makeText(getApplicationContext(), "Error while saving image !", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            Toast.makeText(getApplicationContext(), "Error loading image !", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        cancel(false);
        super.onBackPressed();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            cancel(false);
        }
        return super.onKeyDown(keyCode, event);
    }

    public void cancel(boolean action) {

        bitmap = null;

        Intent intent = new Intent(getApplicationContext(), CameraPreview.class);

        if (!s_type_doc.getType().equals("default") && action) {
            intent = new Intent(getApplicationContext(), Information_preview.class);
        }

        intent.putExtra("s_type_doc", s_type_doc);

        if (s_type_doc.getType().equals("default")) {
            intent = null;
        } else {
            startActivity(intent);
        }

        finish();
    }

}
