package com.esir.orange.ocridentity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.esir.orange.ocridentity.ocrClasses.CustomDocumentsList;
import com.esir.orange.ocridentity.ocrClasses.FilesManagement;
import com.esir.orange.ocridentity.ocrClasses.Scan;
import com.esir.orange.ocridentity.typedoc.DocumentType;
import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.File;
import java.util.List;
import java.util.Stack;

/*
* Created by Oracion
*/

public class DocumentsList extends ActionBarActivity {

    public static TessBaseAPI baseAPI;
    private ImageButton btn_new_scan;
    private ListView docListView;
    private CustomDocumentsList listAdapter;
    public static List<Scan> all_scan = null;
    public static Scan current_doc = null;
    public static String currentPath;
    boolean isConnectedInternet = false;
    private Stack<String> mStackscan = new Stack<String>();
    String TAG = "DocumentList";
    FilesManagement fm = new FilesManagement();
    DocumentType s_type_doc;
    String type_doc = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_documents_list);

        isConnectedInternet = fm.checkInternetConnection(getApplicationContext());

        btn_new_scan = (ImageButton) findViewById(R.id.btn_scan_doc_welcome);

        docListView = (ListView) findViewById(R.id.list_documents_scan);

        s_type_doc = (DocumentType) getIntent().getSerializableExtra("s_type_doc");
        type_doc = getIntent().getStringExtra("type_doc");

        // displayData(type_doc);
        currentPath = fm.getDOCS_PATH() + File.separator + type_doc;
        fm.loadScan(getApplicationContext(), currentPath);

        listAdapter = new CustomDocumentsList(this, all_scan);

        if (all_scan.size() > 0) {
            docListView.setVisibility(View.VISIBLE);
        } else {
            docListView.setVisibility(View.INVISIBLE);
        }

        docListView.setAdapter(listAdapter);

        registerForContextMenu(btn_new_scan);
        registerForContextMenu(docListView);

        if (isConnectedInternet) {
            if (MainActivity.cloudorange != null) {
                if (MainActivity.cloudorange.isConnect()) {
                    MainActivity.cloudorange.browseFolders(null);
                }
            }
        }

        //Start Camera
        btn_new_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                current_doc = new Scan();
                Intent intent = new Intent(getApplicationContext(), CameraPreview.class);
                intent.putExtra("s_type_doc", s_type_doc);
                startActivity(intent);
            }
        });

        docListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i(TAG, "Click item");
                Scan scan = (Scan) parent.getAdapter().getItem(position);

                if (scan.getType().equals("picture")) {
                    viewEditInformation(position);
                } else {
                    mStackscan.push(scan.getPath());
                    currentPath = scan.getPath();
                    displayData(scan.getPath());
                }
            }
        });

    }

    @Override
    public void onResume() {
        displayData(currentPath);
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (!mStackscan.isEmpty()) {
            String path = mStackscan.pop();
            int lastIndex = path.lastIndexOf(File.separator);
            path = path.substring(0, lastIndex);
            currentPath = path;
            displayData(path);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v == docListView) {
            menu.setHeaderTitle("Document Options");
            menu.add(Menu.NONE, 1, 1, "Delete");
            menu.add(Menu.NONE, 2, 2, "Send to Cloud");

            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;

            Scan s = (Scan) docListView.getItemAtPosition(info.position);

            if (s.getType().equals("picture")) {
                menu.add(Menu.NONE, 3, 3, "View/Edit information");
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {

            case R.id.action_logout: // logout cloud
                MainActivity.cloudorange.logout();
                break;

            case R.id.menu_add_folder: // create folder

                AlertDialog.Builder alert = new AlertDialog.Builder(DocumentsList.this);
                alert.setTitle(R.string.action_add_folder);
                final EditText input = new EditText(DocumentsList.this);
                alert.setView(input);
                alert.setPositiveButton(R.string.alert_add_folder_button_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Editable value = input.getText();
                        String folderName = value.toString();
                        File files = new File(currentPath + File.separator + folderName);
                        Log.i("folder", currentPath + File.separator + folderName);
                        fm.createDirectoryOnPhone(files);
                        displayData(currentPath);

                       /* if (isConnectedInternet) {
                            if (MainActivity.cloudorange != null) {
                                MainActivity.cloudorange.createFolder(folderName);
                            }
                        }*/
                    }
                });
                alert.setNegativeButton(R.string.alert_add_folder_button_cancel, null);
                alert.show();

                break;

            case R.id.menu_app_settings: // view/edit settings
                Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
                break;

            default:
                break;
        }
        return true;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        int id = item.getItemId();

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();


        switch (id) {
            case 1: // delete folder or file from phone
                current_doc = (Scan) docListView.getItemAtPosition(info.position);


                AlertDialog.Builder dialog = new AlertDialog.Builder(this);

                dialog.setTitle("Delete from SD card");
                dialog.setIcon(R.drawable.abc_ic_clear_mtrl_alpha);
                dialog.setMessage("Would you like to delete ?");

                dialog.setCancelable(false);

                dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        current_doc = null;
                    }
                });

                dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (fm.deleteOnPhone(current_doc.getPath())) {
                            displayData(currentPath); // update the listView
                        } else {
                            Toast.makeText(getApplicationContext(), "Can't delete !", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                dialog.show();

                break;

            case 2:
                break;

            case 3: // view/edit information about current picture stored in .txt file on SD card
                viewEditInformation(info.position);
                break;

            default:
                break;

        }
        return super.onContextItemSelected(item);
    }

    public void viewEditInformation(int position) {
        try {
            Intent intent = new Intent(getApplicationContext(), Information_preview.class);

            Information_preview.capture = 0;
            current_doc = (Scan) docListView.getItemAtPosition(position);

            if (s_type_doc.getType().equals("default")) {
                Image_preview.bitmap = BitmapFactory.decodeFile(current_doc.getPath());
                intent = new Intent(getApplicationContext(), Image_preview.class);
            }

            intent.putExtra("s_type_doc", s_type_doc);

            startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error while loading information !", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_documents_list, menu);
        return true;
    }

    //TODO see how to display data
    public void displayData(String namefolder) {

        fm.loadScan(getApplicationContext(), namefolder);

        if (all_scan.size() > 0) {
            docListView.setVisibility(View.VISIBLE);
        } else {
            docListView.setVisibility(View.INVISIBLE);
        }

        listAdapter.updateListView(all_scan);
    }

}
