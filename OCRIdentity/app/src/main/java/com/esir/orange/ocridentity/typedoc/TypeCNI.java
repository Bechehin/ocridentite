package com.esir.orange.ocridentity.typedoc;

import android.util.Log;

import com.esir.orange.ocridentity.R;
import com.esir.orange.ocridentity.ocrClasses.ScanInformation;


/**
 * Created by Jean_jean on 2/2/2015.
 */
public class TypeCNI extends DocumentType {

    private String TAG = "CNI decode ";
    ScanInformation information = new ScanInformation();

    public TypeCNI() {
        //set(width,height)
        this.boxHeight = 80;
        this.boxWidth = 450;

        this.type = "cni";
        this.overlay = R.drawable.cin;
    }

    @Override
    public int getBoxWidth(int screenWidth, int defaultWidth) {
        // Numbers coming from ??
        int calculatedWidth = (int)(screenWidth * 35 / 50);
        return calculatedWidth;
    }

    @Override
    public int getBoxHeight(int screenWidth, int defaultWidth) {
        // Numbers coming from ??
        int calculatedHeight = (int)((getBoxWidth(screenWidth, 0) * 6.8) / 10);
        return calculatedHeight;
    }

    public boolean algorithm(String decode) {
        decode = decode.replaceAll(" ", "");  // on retire les espaces et quotes
        decode = decode.replaceAll("`", "");  // on retire les espaces et quotes
        decode = decode.replaceAll("'", "");  // on retire les espaces et quotes
        decode = decode.replaceAll("�", "");  // on retire les espaces et quotes

        String decodeLigne1="";
        String decodeLigne2="";


        int numberOfLignes = decode.length() - decode.replace("\n", "").length() + 1;

        if (numberOfLignes == 2) {
            decodeLigne1 = decode.substring(0,decode.lastIndexOf("\n")+1);  // on retire les /n
            decodeLigne2 = decode.substring(decode.lastIndexOf("\n")+1);  // on retire les /n

        }else  if (numberOfLignes == 3) {

            decodeLigne1 = decode.substring(0,decode.indexOf("\n")+1);  // on retire les /n
            decodeLigne2 = decode.substring(decode.indexOf("\n")+1, decode.lastIndexOf("\n")+1);  // on retire les /n
            String decodeLigne3 = decode.substring(decode.lastIndexOf("\n")+1);  // on retire les /n


            if (decodeLigne1.length() < 30  && decodeLigne2.length() > 30 && decodeLigne3.length() >30)
            {
                decodeLigne1 = decodeLigne2;
                decodeLigne2 = decodeLigne3;
            }
        }

        decodeLigne1 = trimL1(decodeLigne1);
        decodeLigne2 = trimL2(decodeLigne2);

        if (decodeLigne1.length() < 36 || decodeLigne1.length() > 40) return false;
        if (decodeLigne2.length() < 36 || decodeLigne2.length() > 40) return false;

        information.setId(correctOCRString2Number(decodeLigne1.substring(0, 2)));

        information.setCodePays(correctOCRSt2StWithoutNumbers(decodeLigne1.substring(2, 5)));


        String firstname = decodeLigne1.substring(5, 30);

        int first = firstname.indexOf("<");  // on recherche la postion du premier <, qui sert de bourrage
        if (first == -1 ) return false;

        first+=5;

        information.setFirstName(correctOCRSt2StWithoutNumbers(decodeLigne1.substring(5, first)).toUpperCase());
        information.setCodeAdministration(correctOCRString2Number(decodeLigne1.substring(30, 36)));



        information.setEmissionDate(correctOCRString2Number(decodeLigne2.substring(0, 4)));
        /*
        if(decode.substring(30, 33).equals(decode.substring(41, 44))){
            Log.i(TAG, "OCR correct");
        }
        */

        information.setDocumentNumber(correctOCRString2Number(decodeLigne2.substring(0, 12)));
        Log.d(TAG, "MANU setDocumentNumber" +decodeLigne2.substring(0, 12));
        //Controle
        // boolean a = controle(decode.substring(36, 48),Integer.parseInt(decode.substring(48, 49)));

        String name = decodeLigne2.substring(13, 27);
        int n = name.indexOf("<");

        if (n==-1) n+=5;
        else
            n +=13;

        name = correctOCRSt2StWithoutNumbers(decodeLigne2.substring(13, n)).toUpperCase();
        information.setName(name);

        //controle
        // boolean b = controle(decode.substring(63, 69),Integer.parseInt(decode.substring(69, 70)));


        StringBuilder tempDate = new StringBuilder();
        String date = decodeLigne2.substring(27, 33);
        date = correctOCRString2Number(date);


        tempDate.append(date.substring(4, 6) + "-" + date.substring(2, 4) + "-" + date.substring(0, 2));
        information.setBirthday(tempDate.toString());

        //information.setSexe(decode.substring(70, 71));
        information.setSexe(decode.substring(34, 35));
        //controle
        String test = decode.substring(0, 36);


        //String test2 = decode.substring(37, 72);
        // test.concat(test2);
        // boolean c = controle(test ,Integer.parseInt(decode.substring(71, 72)));
        // return a&b&c;
        return true;
    }

    @Override
    public boolean controle(String decode, int cle) {
        int[] facteurs = {7, 3, 1};
        int resultat = 0;
        int valeur =0;

        char[] chararray = decode.toCharArray();

        for(int i=0; i<chararray.length;i++){
            char c = chararray[i];
            if(c == '<'){
                valeur = 0;
            }else if(c>= 'A' && c <= 'Z'){
                int codeASCII = (int) c;
                valeur = codeASCII - 55;
            }else if(c >= '0' && c <= '9'){
                valeur = (int) c - 48;
            }
            resultat = valeur * facteurs[i % 3];
        }
        int cleDeControle = resultat % 10;
        return cle == cleDeControle;
    }

    private String trimL1(String inputString)
    {
        String trimedSt= inputString;
        int idx = trimedSt.indexOf("ID");

        if (idx >-1 && idx !=0 && idx <6)
        {
            trimedSt = trimedSt.substring(idx, trimedSt.length());
            Log.e(TAG," After trim ID" + trimedSt );
        }

        return trimedSt;
    }



    // L2 doit contenir 36 caracters
    // On essaie de nettoyer un peu la chaine de caraceteres si on a scann� trop de carateres
    /*
    1�4	Chiffres	Ann�e (1�2) et mois (3�4) d'�mission de la carte d'identit�.
    5�7	Alphanum�rique	M�me chose que les caract�res 31�33 sur la premi�re ligne.
    8�12	Chiffres	Attribu� par le centre de gestion par ordre chronologique par rapport au lieu de d�livrance et � la date de demande.
    13	Chiffre	Cl� de contr�le des caract�res 1�12.
    14�27	Lettres	Pr�nom du possesseur de la carte, suivi du second pr�nom s�par� par deux <. En cas de pr�nom compos�, le trait d'union est pr�sent� par un <.
    28�33	Chiffres	Date de naissance (AAMMJJ).
    34	Chiffre	Cl� de contr�le des chiffres 28�33.
    35	Lettre	Sexe (M ou F).
    36	Chiffre	Cl� de contr�le de toute la premi�re ligne, suivie des 35 premiers caract�res de la deuxi�me ligne.

    */

    private String trimL2(String inputString)
    {
        String trimedSt= inputString;

        int idx = trimedSt.lastIndexOf("F");
        int idx1 = trimedSt.lastIndexOf("M");

        if  (trimedSt.length() > 30 )
        {

            int maxIdx = Math.max(idx,idx1);

            if (maxIdx > 34) {

                trimedSt = trimedSt.substring(0 , maxIdx + 2);   // on retire tous les carateres apres la postion sexe +1
                if (trimedSt.length() >36 )
                    trimedSt = trimedSt.substring(trimedSt.length() - 36 , maxIdx + 2); // on retire tous les carateres apres la postion sexe +1

            }


            Log.e(TAG," After trim ID" + trimedSt );
        }

        return trimedSt;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public ScanInformation getScanInformation() {
        return information;
    }
}
