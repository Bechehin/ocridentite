package com.esir.orange.ocridentity.cloud;

import android.content.Context;

import com.orange.labs.sdk.session.AuthSession;

import java.io.File;

/**
 * Created by Jean_jean on 1/13/2015.
 */
public interface Interface_Cloud {

    public void createFolder(String name);

    public void logout();

    public void uploadPicture(File sourceFile, Context m_context);
}
