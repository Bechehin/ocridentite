package com.esir.orange.ocridentity.typedoc;

import android.graphics.Point;

import com.esir.orange.ocridentity.ocrClasses.ScanInformation;

/**
 * Created by Jean_jean on 2/20/2015.
 */
public class TypeDefault extends DocumentType {

    public TypeDefault() {
        //set(width,height)
        boxHeight = 20;
        boxWidth = 20;

        type = "default";
    }

    @Override
    public boolean algorithm(String decode) throws IndexOutOfBoundsException, NumberFormatException {
        return false;
    }

    @Override
    public boolean controle(String decode, int cle) {
        return false;
    }

    @Override
    public ScanInformation getScanInformation() {
        return null;
    }

}
