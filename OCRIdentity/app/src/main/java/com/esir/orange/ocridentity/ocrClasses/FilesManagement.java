package com.esir.orange.ocridentity.ocrClasses;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.esir.orange.ocridentity.DocumentsList;
import com.esir.orange.ocridentity.MainActivity;
import com.esir.orange.ocridentity.typedoc.DocumentType;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Oracion on 04/12/2014.
 * Class for manage file and folder in phone
 * Create folder for save picture
 */
public class FilesManagement {

    // project files directory
    private String PROJECT_FILES_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "OCRIdentity";

    // ocr files directory
    private String TESS_FILES_PATH = PROJECT_FILES_PATH + File.separator + "tesseract";

    // ocr data files directory
    private String TESS_LANG_DATA_PATH = TESS_FILES_PATH + File.separator + "tessdata";

    // project scanned files directory
    private String DOCS_PATH = PROJECT_FILES_PATH + File.separator + "files";

    private String TAG = "FileManagement";


    // this function creates a directory on your SD card
    public boolean createDirectoryOnPhone(File path) {
        try {

            return (!path.exists()) ? path.mkdir() : true;

        } catch (Exception e) {
            Log.e("Error Folder Create : ", path.getAbsolutePath() + " : " + e.getMessage());
            return false;
        }
    }

    // this function checks the internet connection of the phone
    public boolean checkInternetConnection(Context context) {
        try {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        } catch (Exception e) {
            return false;
        }
    }

    // this function deletes all folder and its sub-folders or file on your SD card
    // it deletes also .txt file associated to the picture if it exists
    public boolean deleteOnPhone(String path) {
        boolean b = true;
        try {
            File file = new File(path);
            if (file.isDirectory()) {
                for (File child : file.listFiles()) {
                    deleteOnPhone(child.getPath());
                }
            }
            b = file.delete();
            String txt = file.getPath().replace(".jpg", ".txt");
            file = new File(txt);
            if (file.exists()) {
                b = file.delete();
            }
        } catch (Exception e) {
            b = false;
        }
        return b;
    }

    // this function copies a file from a stream to another
    public boolean copyFile(InputStream in, OutputStream out) {
        try {

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            return true;

        } catch (Exception e) {
            Log.e("Error Copy File : ", e.getMessage());
            return false;
        }
    }

    // this function creates essential directory for the project
    public boolean createAllProjectDirectories() {
        try {

            // check if there is a mounted media in the phone
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {

                File dataDirectory = new File(PROJECT_FILES_PATH);

                // create project files directory
                if (createDirectoryOnPhone(dataDirectory)) {

                    dataDirectory = new File(TESS_FILES_PATH);
                    File docDirectory = new File(DOCS_PATH);

                    // Create now ocr data and scanned documents directories
                    if (createDirectoryOnPhone(dataDirectory) && createDirectoryOnPhone(docDirectory)) {

                        dataDirectory = new File(TESS_LANG_DATA_PATH);

                        // Create now ocr languages data directory
                        if (createDirectoryOnPhone(dataDirectory)) {
                            return true;
                        } else {
                            return false;
                        }

                    } else {
                        return false;
                    }

                } else {
                    return false;
                }

            } else {
                Log.e("Error SD card Mounted", "The SD card is unmounted...");
                return false;
            }

        } catch (Exception e) {
            return false;
        }
    }

    // this function copies all orc language files from assets to SD card if not exist before
    public boolean copyLanguageFiles(Context context) {

        boolean b = true;

        try {

            if (createAllProjectDirectories()) {

                String[] files = null;

                AssetManager assetManager = context.getAssets();
                files = assetManager.list("tesseract_lang");

                for (String file : files) {

                    boolean temp = new File(TESS_LANG_DATA_PATH + "/" + file).exists();

                    if (!temp) {
                        InputStream in = assetManager.open("tesseract_lang/" + file);
                        OutputStream out = new FileOutputStream(TESS_LANG_DATA_PATH + "/" + file);

                        b = copyFile(in, out);

                        in.close();
                        in = null;
                        out.flush();
                        out.close();
                        out = null;
                    }
                }
            } else {
                b = false;
            }

        } catch (Exception e) {
            b = false;
            Log.e("Error Copy Language : ", e.getMessage());
        }

        return b;
    }

    public void setDocPath(String path) {
        DOCS_PATH = path;
    }

    public String getDOCS_PATH() {
        return DOCS_PATH;
    }

    public boolean savePhoto(Context context, Bitmap bm, String chemin) {
        FileOutputStream image = null;
        String filename = chemin + File.separator + getFileName();
        File file = new File(filename);
        try {
            image = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        bm.compress(Bitmap.CompressFormat.JPEG, 100, image);

        boolean isConnectedInternet = checkInternetConnection(context);

        Log.i(TAG, "Connect = " + isConnectedInternet);
        //Save in cloud
        //TODO send sign
        if (isConnectedInternet) {
            if (MainActivity.cloudorange != null) {
                MainActivity.cloudorange.save_file(context, file, bm);
            }
        }


        if (bm != null) {
            DocumentsList.current_doc.setImage(bm);
            DocumentsList.current_doc.setName(filename);
            DocumentsList.current_doc.setPath(filename);
        } else {
            DocumentsList.current_doc = null;
        }

        return bm != null;
    }

    public static String getFileName() {
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        return "IMG" + timeStampFormat.format(new Date()) + ".jpg";
    }

    // load all files or folders saved by users
    public void loadScan(Context context, String pathF) {
        try {

            DocumentsList.all_scan = new ArrayList<Scan>();

            String[] files = null;
            File directory = new File(pathF);

            if (!directory.exists()) {
                createDirectoryOnPhone(directory);
            }

            files = directory.list();

            for (String file : files) {

                if (!file.endsWith(".txt")) {
                    Scan s = new Scan();
                    String path = pathF + File.separator + file;
                    s.setPath(path);
                    s.setName(file);

                    if (file.endsWith(".jpg")) {
                        s.setImage(BitmapFactory.decodeFile(path));
                        s.setCreate_date(getTrueDate(file.substring(3, 11), 2));
                        s.setType("picture");
                    } else {
                        s.setType("folder");
                        Log.i(TAG, "Load scan folder");
                    }
                    DocumentsList.all_scan.add(s);
                }
            }

        } catch (Exception e) {
            Toast.makeText(context.getApplicationContext(), "Error loading scans !", Toast.LENGTH_SHORT).show();
        }
    }

    // read information on picture after capture
    public ScanInformation extractInformation(DocumentType d_type) {

        try {

            String info = null;

            if (DocumentsList.baseAPI != null) {

                DocumentsList.baseAPI.setVariable("textord_max_noise_size", "2");
                DocumentsList.baseAPI.setVariable("textord_heavy_nr ", "1");
                DocumentsList.baseAPI.setVariable("tessedit_ocr_engine_mode", "2");

           //     Bitmap bitmap = DocumentsList.current_doc.getImage();

              /*  TraitementImage traitementImage = new TraitementImage();

                bitmap = traitementImage.rotate(bitmap, DocumentsList.current_doc.getPath());
                Bitmap photo = bitmap.copy(bitmap.getConfig(), true);

                // Applique un flou gaussien sur la photo
                photo = traitementImage.applyGaussianBlur(photo);

                // Sharpening de la photo
                photo = traitementImage.sharpen(photo, 1.5);

                // Améliore les contours
                photo = traitementImage.smooth(photo, 5.0);*/

                DocumentsList.baseAPI.setImage(DocumentsList.current_doc.getImage());
                info = DocumentsList.baseAPI.getUTF8Text();
                Log.i(TAG, "Decode Info : " + info);

                //TODO check test if controle is ok or not
                d_type.algorithm(info);
                return d_type.getScanInformation();
            } else {
                // Process France Passport
                return null;
            }

        } catch (Exception e) {
            Log.e(TAG, "Error image processing : " + e.getMessage());
            return null;
        }
    }

    public String getTrueDate(String dateT, int offset) {
        String dateR = "";
        try {

            dateR = dateT.substring(dateT.length() - 2, dateT.length());
            dateR = dateR + "/" + dateT.substring(dateT.length() - 4, dateT.length() - 2);
            dateR = dateR + "/" + dateT.substring(0, 2 + offset);

        } catch (Exception e) {
        }
        return dateR;
    }

    public boolean saveInformationAsText(String filePath, ScanInformation info, DocumentType d_type) {

        if (!d_type.getType().equals("default")) {
            try {
                filePath = filePath.replace(".jpg", ".txt");
                File myFile = new File(filePath);
                if (!myFile.exists()) {
                    myFile.createNewFile();
                }
                FileOutputStream fOut = new FileOutputStream(myFile);
                OutputStreamWriter osw = new OutputStreamWriter(fOut);
                String line = System.getProperty("line.separator");
                osw.append(info.getCodePays() + line);
                osw.append(info.getName() + line);
                osw.append(info.getFirstName() + line);
                osw.append(info.getBirthday() + line);
                osw.append(info.getDocumentNumber() + line);
                osw.append(info.getType() + line);
                osw.append(info.getExpirationDate() + line);
                osw.flush();
                osw.close();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    public ScanInformation readInformationFromText(Context context, DocumentType documentType, String filePath) {
        try {

            filePath = filePath.replace(".jpg", ".txt");
            File myFile = new File(filePath);

            if (!myFile.exists()) {
                return extractInformation(documentType);
            } else {

                ScanInformation info = new ScanInformation();

                BufferedReader objBufferReader = new BufferedReader(new FileReader(myFile));

                String strLine;

                if ((strLine = objBufferReader.readLine()) != null) {
                    info.setCodePays(strLine);
                }

                if ((strLine = objBufferReader.readLine()) != null) {
                    info.setName(strLine);
                }

                if ((strLine = objBufferReader.readLine()) != null) {
                    info.setFirstName(strLine);
                }

                if ((strLine = objBufferReader.readLine()) != null) {
                    info.setBirthday(strLine);
                }

                if ((strLine = objBufferReader.readLine()) != null) {
                    info.setDocumentNumber(strLine);
                }
                if ((strLine = objBufferReader.readLine()) != null) {
                    info.setType(strLine);
                }

                if ((strLine = objBufferReader.readLine()) != null) {
                    info.setExpirationDate(strLine);
                }

                objBufferReader.close();

                return info;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getPROJECT_FILES_PATH() {
        return PROJECT_FILES_PATH;
    }

    public String getTESS_FILES_PATH() {
        return TESS_FILES_PATH;
    }

    public String getTESS_LANG_DATA_PATH() {
        return TESS_LANG_DATA_PATH;
    }

}
