package com.esir.orange.ocridentity.ocrClasses;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.util.Log;

import com.esir.orange.ocridentity.DocumentsList;
import com.esir.orange.ocridentity.typedoc.DocumentType;

import java.io.File;
import java.io.IOException;

/**
 * Created by Jean_jean on 2/24/2015.
 */
public class TraitementImage {

    private String TAG = "TraitementImage";
    // =============================================================
    // ========== Methodes de traitement de l'image ================
    // =============================================================

    // Binarization
    public Bitmap colorToBlackAndWhite(Bitmap bitmap)
    {
        Log.i(TAG, "Transforme into b&w...");
        Paint paint = new Paint();
        Canvas canvas = new Canvas(bitmap);
        ColorMatrix cm = new ColorMatrix();
        float a = 77f;
        float b = 151f;
        float c = 28f;
        float t = 120 * -256f;
        cm.set(new float[]
                { a, b, c, 0, t, a, b, c, 0, t, a, b, c, 0, t, 0, 0, 0, 1, 0 });
        paint.setColorFilter(new ColorMatrixColorFilter(cm));
        canvas.drawBitmap(bitmap, 0, 0, paint);
        Log.i(TAG, "... done");
        return bitmap;
    }

    // Adoucissement
    public Bitmap smooth(Bitmap src, double value)
    {
        ConvolutionMatrix convMatrix = new ConvolutionMatrix(3);
        convMatrix.setAll(1);
        convMatrix.Matrix[1][1] = value;
        convMatrix.Factor = value + 8;
        convMatrix.Offset = 1;
        Log.i(TAG, "smooth");
        return ConvolutionMatrix.computeConvolution3x3(src, convMatrix);
    }

    // Flou gaussien
    public Bitmap applyGaussianBlur(Bitmap src)
    {
        double[][] GaussianBlurConfig = new double[][]
                {
                        { 1, 2, 1 },
                        { 2, 4, 2 },
                        { 1, 2, 1 } };
        ConvolutionMatrix convMatrix = new ConvolutionMatrix(3);
        convMatrix.applyConfig(GaussianBlurConfig);
        convMatrix.Factor = 16;
        convMatrix.Offset = 0;
        Log.i(TAG, "gaussian blur");
        return ConvolutionMatrix.computeConvolution3x3(src, convMatrix);
    }

    // Rendre plus net
    public Bitmap sharpen(Bitmap src, double weight)
    {
        double[][] SharpConfig = new double[][]
                {
                        { 0, -2, 0 },
                        { -2, weight, -2 },
                        { 0, -2, 0 } };
        ConvolutionMatrix convMatrix = new ConvolutionMatrix(3);
        convMatrix.applyConfig(SharpConfig);
        convMatrix.Factor = weight - 8;
        return ConvolutionMatrix.computeConvolution3x3(src, convMatrix);
    }

    public Bitmap rotate(Bitmap bitmap,String DOCS_PATH){
        try
        {
            ExifInterface exif = new ExifInterface(DOCS_PATH);
            int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            Log.i(TAG, "Orient: " + exifOrientation);

            int rotate = 0;
            // Rotation de l'ecran (format portrait/paysage/renverse/etc.)
            switch (exifOrientation)
            {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
            }

            Log.i(TAG, "Rotation: " + rotate);

            if (rotate != 0)
            {
                // Getting width & height of the given image.
                int w = bitmap.getWidth();
                int h = bitmap.getHeight();

                // Setting pre rotate
                Matrix mtx = new Matrix();
                mtx.preRotate(rotate);

                // Rotating Bitmap
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, false);
            }

            // Convert to ARGB_8888, required by tess
            bitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);

        } catch (IOException e)
        {
            Log.i(TAG, "Couldn't correct orientation: " + e.toString());
        }
        return bitmap;
    }


}
