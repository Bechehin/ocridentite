package com.esir.orange.ocridentity.certification;

import com.esir.orange.ocridentity.ocrClasses.FilesManagement;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;

/**
 * Created by Jean_jean on 2/10/2015.
 * Certification a documents hash and sign
 * TODO catch exeption
 */
public class Certifiaction {

    PrivateKey priv = null;
    PublicKey pub = null;

    /**
     * Generate key and gen clef
     * @throws NoSuchProviderException
     * @throws NoSuchAlgorithmException
     */
    public void create_clef() throws NoSuchProviderException, NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(1024);
        KeyPair pair = keyGen.generateKeyPair();
        priv = pair.getPrivate();
        pub = pair.getPublic();
    }

    /**
     * Hash and sign the byte[]
     * @param picture
     * @return the file where is the picture
     * @throws NoSuchAlgorithmException
     * @throws SignatureException
     * @throws InvalidKeyException
     */
    public String hash_picture(byte[] picture, String name) throws NoSuchAlgorithmException, SignatureException, InvalidKeyException, IOException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        byte[] hash_picture = messageDigest.digest(picture);
        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initSign(priv);
        signature.update(hash_picture);
        byte[] sign_picture = signature.sign();
        FileOutputStream sigfos = new FileOutputStream(name);
        sigfos.write(sign_picture);
        sigfos.close();
        return name;
    }

    public PublicKey getPub() {
        return pub;
    }
}
