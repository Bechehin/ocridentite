package com.esir.orange.ocridentity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.KeyEvent;

import com.esir.orange.ocridentity.ocrClasses.OcrParameters;

/**
 * Created by Oracion on 27/01/2015.
 */

public class SettingsActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    public static void loadPreferences(Context context) {

        try {

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

            OcrParameters.OCR_LANGUAGE = prefs.getString("lang", "fra");
            OcrParameters.keepMeLogIn = prefs.getBoolean("logIn", false);

            String listPrefs = prefs.getString("sync", "1");

            OcrParameters.isSynchronize = new Integer(listPrefs);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
