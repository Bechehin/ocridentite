package com.esir.orange.ocridentity.cloud;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ListView;

import com.esir.orange.ocridentity.MainActivity;
import com.esir.orange.ocridentity.certification.Certifiaction;
import com.esir.orange.ocridentity.ocrClasses.OcrParameters;
import com.orange.labs.sdk.OrangeCloudAPI;
import com.orange.labs.sdk.OrangeListener;
import com.orange.labs.sdk.exception.OrangeAPIException;
import com.orange.labs.sdk.session.AuthSession;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Stack;

/**
 * Created by Jean_jean on 1/12/2015.
 * Class to connect Cloud Orange
 */
public class CloudOrange implements Interface_Cloud {

    private String TAG = "CloudOrange";

    private OrangeCloudAPI<AuthSession> mApi;
    private Stack<OrangeCloudAPI.Entry> mStack;
    private HashMap<String, OrangeCloudAPI.Entry> mMap;
    //API Cloud
    private String APP_KEY = "uJzPtzlgDnf1Hxl7OqO9HEzQu7vVdel3";
    private String APP_SECRET = "VJxxGbe3z0hgIWfV";
    private String APP_REDIRECT_URI = "ocrapplication://";

    private Certifiaction certifiaction;

    private boolean isConnect = false;

    //TODO check browse folder when not root
    public CloudOrange(Context context) {
        AuthSession session = new AuthSession(context, APP_KEY, APP_SECRET, APP_REDIRECT_URI);
        mApi = new OrangeCloudAPI<AuthSession>(session);
        mApi.getSession().startAuthentication();
        mStack = new Stack<OrangeCloudAPI.Entry>();
        mMap = new HashMap<String, OrangeCloudAPI.Entry>();
        certifiaction = new Certifiaction();
    }

    public AuthSession getSession() {
        return mApi.getSession();
    }

    /**
     * Method to browse folders
     *
     * @param entry an entry Object or null
     */
    public void browseFolders(OrangeCloudAPI.Entry entry) {
        // Display loader and make Cloud API request
        mApi.listFolder(entry, new OrangeListener.Success<OrangeCloudAPI.Entry>() {
            @Override
            public void onResponse(OrangeCloudAPI.Entry response) {
                // Add new result in stack and display data in the listView
                mStack.push(response);
                Log.i(TAG, "Browse folder name " + response.name);
                mMap.put(response.name, response);
            }
        }, new OrangeListener.Error() {
            @Override
            public void onErrorResponse(OrangeAPIException error) {
                Log.i(TAG, error.toString());
            }
        });
    }

    public void browseFolders(String sEntry, String st) {
        // Display loader and make Cloud API request
        Log.i(TAG, "Browse new folder name " + mStack.lastElement().name + " contains " + mMap.containsKey(sEntry));
        if (mMap.containsKey(sEntry)) {
            OrangeCloudAPI.Entry entry = mMap.get(sEntry);
            Log.i(TAG, "Browse new folder");
            mApi.listFolder(entry, new OrangeListener.Success<OrangeCloudAPI.Entry>() {
                @Override
                public void onResponse(OrangeCloudAPI.Entry response) {
                    // Add new result in stack and display data in the listView
                    mStack.push(response);
                    Log.i(TAG, response.name);
                }
            }, new OrangeListener.Error() {
                @Override
                public void onErrorResponse(OrangeAPIException error) {
                    Log.i(TAG, error.toString());
                }
            });
        }
    }

    public void createFolder(String name) {
        OrangeCloudAPI.Entry entry = mStack.lastElement();
        if (!TextUtils.isEmpty(name)) {
            mApi.createFolder(entry, name, new OrangeListener.Success<OrangeCloudAPI.Entry>() {
                @Override
                public void onResponse(OrangeCloudAPI.Entry response) {
                    mMap.put(response.name, response);
                }
            }, new OrangeListener.Error() {
                @Override
                public void onErrorResponse(OrangeAPIException error) {
                    Log.i(TAG, error.toString());
                }
            });
        }
    }

    /**
     * Called when users clicks on logout Menu
     * Logout user and display authentication Activity.
     */
    public void logout() {
        // Clean session
        AuthSession session = mApi.getSession();
        // To force reload of ListFragment
        cleanData();
        mApi.unlink();
        session.startAuthentication();
    }

    public boolean isConnect() {
        return isConnect;
    }

    public void setConnect(boolean isConnect) {
        this.isConnect = isConnect;
    }


    /**
     * Clean Data (stack) and refresh layout (listView)
     */
    private void cleanData() {
        mStack = new Stack<OrangeCloudAPI.Entry>();
    }

    /**
     * Upload a file
     * do this function in background
     * TODO check for floder
     */
    public void uploadPicture(File sourceFile, Context m_context) {
        OrangeCloudAPI.Entry entry = mStack.lastElement();
        UploadPicture uploadPicture = new UploadPicture(m_context, mApi, entry, sourceFile);
        Log.i(TAG, "Upload picture on cloud");
        uploadPicture.execute();
    }

    public void save_file(Context context, File filename, Bitmap bitmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        if (isConnect()) {
            uploadPicture(filename, context);
            try {
                String new_name = filename.toString().replace(".jpg", ".cert");
                String name = certifiaction.hash_picture(byteArray, new_name);
                File file = new File(name);
                if (!file.exists()) {
                    file.createNewFile();
                }
                uploadPicture(file, context);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (SignatureException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            ExifInterface exifInterface = null;
            try {
                exifInterface = new ExifInterface(filename.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (exifInterface != null) {
                exifInterface.setAttribute(ExifInterface.TAG_MODEL, "true");
                try {
                    exifInterface.saveAttributes();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.i(TAG, "Meta data new " + exifInterface.getAttribute(ExifInterface.TAG_MODEL));
            }
        }
    }
}
