package com.esir.orange.ocridentity.ocrClasses;

/**
 * Created by Oracion on 30/01/2015.
 */
public class OcrParameters {

    // ocr language. "fra" (french) can replace "eng" (english) if you want to
    public static String OCR_LANGUAGE = "fra";

    public static int isSynchronize;

    public static boolean keepMeLogIn;

}
