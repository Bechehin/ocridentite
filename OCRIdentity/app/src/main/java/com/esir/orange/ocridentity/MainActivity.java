package com.esir.orange.ocridentity;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.esir.orange.ocridentity.cloud.CloudOrange;
import com.esir.orange.ocridentity.ocrClasses.FilesManagement;
import com.esir.orange.ocridentity.typedoc.TypeCNI;
import com.esir.orange.ocridentity.typedoc.TypeDefault;
import com.esir.orange.ocridentity.typedoc.TypePasseport;
import com.esir.orange.ocridentity.typedoc.TypePermis;
import com.orange.labs.sdk.OrangeListener;
import com.orange.labs.sdk.exception.OrangeAPIException;
import com.orange.labs.sdk.session.AuthSession;

/**
 * Created by Jean_jean on 2/2/2015.
 * Display the different type of document
 */
public class MainActivity extends ActionBarActivity {

    //Button for different doc
    private ImageButton btn_passeport;
    private ImageButton btn_cni;
    private RelativeLayout relativeLayout;

    private String TAG = "main activity";

    //To know if connected to cloud and internet
    boolean isConnectedInternet = false;

    //Cloud orange
    public static CloudOrange cloudorange = null;
    FilesManagement fm = new FilesManagement();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        isConnectedInternet = fm.checkInternetConnection(getApplicationContext());

        //Connexion au cloud

        GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(new ImageButtonAdapter(this));

        //listener on button to start Activity DocumentList Add extra for know document type
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Intent documents = new Intent(getApplicationContext(), DocumentsList.class);
                Log.i(TAG, Integer.toString(position));
                if (position == 0) {
                    TypePasseport passeport = new TypePasseport();
                    documents.putExtra("s_type_doc", passeport);
                    documents.putExtra("type_doc", "passeport");
                    startActivity(documents); //let's start the new activity
                } else if (position == 1) {
                    TypeCNI cni = new TypeCNI();
                    documents.putExtra("s_type_doc", cni);
                    documents.putExtra("type_doc", "cni");
                    startActivity(documents);
                }
                else if (position == 2) {
                    TypePermis permis = new TypePermis();
                    documents.putExtra("s_type_doc", permis);
                    documents.putExtra("type_doc", "permis");
                    startActivity(documents);
                }
                else if (position == 3) {
                    TypeDefault defau = new TypeDefault();
                    documents.putExtra("s_type_doc", defau);
                    documents.putExtra("type_doc", "default");
                    startActivity(documents);
                }
            }

        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isConnectedInternet) {
            if (cloudorange != null) {
                final AuthSession session = cloudorange.getSession();
                session.checkAuthentication(new OrangeListener.Success<String>() {
                    @Override
                    public void onResponse(String response) {
                        cloudorange.setConnect(true);
                    }
                }, new OrangeListener.Error() {
                    @Override
                    public void onErrorResponse(OrangeAPIException error) {
                        // An error occurred
                        Log.e(TAG, error.toString());
                    }
                });
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menu_settings:
                Intent i = new Intent(this, SettingsActivity.class);
                startActivity(i);
                break;
            case R.id.menu_login:
                if (isConnectedInternet) {
                    cloudorange = new CloudOrange(getApplicationContext());
                } else {
                    Toast error = Toast.makeText(getApplicationContext(), "Not connect", Toast.LENGTH_LONG);
                    error.show();
                }
                break;

        }

        return true;
    }

}


