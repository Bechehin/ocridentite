package com.esir.orange.ocridentity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.esir.orange.ocridentity.ocrClasses.FilesManagement;
import com.esir.orange.ocridentity.ocrClasses.ScanInformation;
import com.esir.orange.ocridentity.typedoc.DocumentType;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Information_preview extends ActionBarActivity {

    private ImageButton btn_ignore, btn_validate;
    private EditText textCode, textName, textFirstName, textBirth, textExpiration, textNumber, textType;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;
    public static int capture = -1;
    boolean test = false;
    DocumentType s_type_doc;
    ScanInformation info = null;
    FilesManagement fm = null;
    String TAG = "Information_preview";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_preview);

        myCalendar = Calendar.getInstance();


        btn_ignore = (ImageButton) findViewById(R.id.btn_ignore);
        btn_validate = (ImageButton) findViewById(R.id.btn_validate);

        textCode = (EditText) findViewById(R.id.country_code);
        textName = (EditText) findViewById(R.id.his_name);
        textFirstName = (EditText) findViewById(R.id.his_firstName);
        textBirth = (EditText) findViewById(R.id.his_birthDate);
        textNumber = (EditText) findViewById(R.id.his_doc_number);
        textType = (EditText) findViewById(R.id.his_doc_type);
        textExpiration = (EditText) findViewById(R.id.his_doc_expiration);

        s_type_doc = (DocumentType) getIntent().getSerializableExtra("s_type_doc");
        fm = new FilesManagement();

        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                if (test) {
                    updateText(textBirth);
                } else {
                    updateText(textExpiration);
                }
            }

        };

        textBirth.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    test = true;
                    showDateInterface();
                }
            }
        });

        textExpiration.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    test = false;
                    showDateInterface();
                }
            }
        });

        textBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                test = true;
                showDateInterface();
            }
        });

        textExpiration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                test = false;
                showDateInterface();
            }
        });

        if (s_type_doc != null && capture == 1) {
            info = s_type_doc.getScanInformation();
        } else {
            info = fm.readInformationFromText(getApplicationContext(), s_type_doc, DocumentsList.current_doc.getPath());
        }

        if (info != null) {
            textCode.setText(info.getCodePays());
            textName.setText(info.getName());
            textFirstName.setText(info.getFirstName());
            textBirth.setText(info.getBirthday());
            textNumber.setText(info.getDocumentNumber());
            textType.setText(info.getType());
            textExpiration.setText(info.getEmissionDate());
        } else {
            Toast.makeText(getApplicationContext(), "Not .txt with this picture", Toast.LENGTH_SHORT).show();
        }

        // cancel operation but image was already saved
        btn_ignore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(Information_preview.this);
                alert.setMessage("Do you want to cancel ? Modifications won't be saved !");
                alert.setIcon(R.drawable.abc_ic_clear_mtrl_alpha);
                alert.setTitle("Information preview");

                alert.setNegativeButton("No", null);

                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        exit();
                    }
                });

                alert.show();
            }
        });


        // save as text after correction and send to cloud if possible
        btn_validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String imageName = DocumentsList.current_doc.getPath();
                    resetInformation();
                    if (fm.saveInformationAsText(imageName, info, s_type_doc)) {
                        Toast.makeText(getApplicationContext(), "Information saved !", Toast.LENGTH_SHORT).show();
                        exit();
                    } else {
                        Toast.makeText(getApplicationContext(), "Error while saving information !", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Error while saving information !", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void updateText(EditText text) {

        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.FRENCH);
        text.setText(sdf.format(myCalendar.getTime()));
    }


    private void resetInformation() {
        info.setCodePays(textCode.getText().toString());
        info.setDocumentNumber(textNumber.getText().toString());
        info.setName(textName.getText().toString());
        info.setFirstName(textFirstName.getText().toString());
        info.setEmissionDate(textExpiration.getText().toString());
        info.setType(textType.getText().toString());
        info.setBirthday(textBirth.getText().toString());
    }

    public void exit() {
        capture = -1;
        DocumentsList.current_doc = null;
        info = null;
        finish();
    }

    @Override
    public void onBackPressed() {
        exit();
        super.onBackPressed();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == event.KEYCODE_BACK) {
            exit();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showDateInterface() {
        new DatePickerDialog(Information_preview.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }
}
