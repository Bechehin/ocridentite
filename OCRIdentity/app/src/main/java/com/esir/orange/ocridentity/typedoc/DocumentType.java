package com.esir.orange.ocridentity.typedoc;

import android.graphics.Canvas;

import com.esir.orange.ocridentity.ocrClasses.ScanInformation;

import java.io.Serializable;

/**
 * Created by Jean_jean on 2/2/2015.
 * Fonction to implemnts for differnets documents
 */
public abstract class DocumentType implements Serializable {

    protected String type;
    protected int overlay = 0;
    protected int boxHeight, boxWidth;

    public abstract boolean algorithm(String decode);
    public abstract boolean controle(String decode, int cle);
    public abstract ScanInformation getScanInformation();

    public int getBoxWidth(int screenWidth, int defaultWidth) {
        return defaultWidth;
    }

    public int getBoxHeight(int screenWidth, int defaultHeight) {
        return defaultHeight;
    }

    public String getType() {
        return type;
    }

    public boolean hasOverlay() {
        return overlay != 0;
    }

    public int getOverlayResource() {
        return overlay;
    }

    // Reformate String with only number characters
    protected String correctOCRString2Number(String input)
    {
        if (input != null)
        {
            String reformatedString;
            reformatedString = input.replace('O', '0');
            reformatedString = reformatedString.replace('o', '0');
            reformatedString = reformatedString.replace('U','0');
            reformatedString = reformatedString.replace('�','0');
            reformatedString = reformatedString.replace('�','0');
            reformatedString = reformatedString.replace('i','1');
            reformatedString = reformatedString.replace('I','1');
            reformatedString = reformatedString.replace('l','1');
            reformatedString = reformatedString.replace('t','1');
            reformatedString = reformatedString.replace('z','2');
            reformatedString = reformatedString.replace('Z','2');
            reformatedString = reformatedString.replace('a','4');
            reformatedString = reformatedString.replace('A','4');
            reformatedString = reformatedString.replace('k','4');
            reformatedString = reformatedString.replace('K','4');
            reformatedString = reformatedString.replace('?','7');
            reformatedString = reformatedString.replace('B','8');
            reformatedString = reformatedString.replace('q','9');
            reformatedString = reformatedString.replace('g','9');
            reformatedString = reformatedString.replaceAll("[^0-9]+", " ");

            return reformatedString;

        }else
            return null;

    }


    protected String correctOCRSt2StWithoutNumbers(String input) {
        if (input != null) {
            String reformatedString;
            reformatedString = input.replace('0', 'O');
            reformatedString = reformatedString.replace('1', 'I');
            reformatedString = reformatedString.replace('8', 'B');
            reformatedString = reformatedString.replace('3', 'B');
            reformatedString = reformatedString.replace('6', 'G');
            reformatedString = reformatedString.replace('5', 'S');
            reformatedString = reformatedString.replaceAll("[^a-zA-Z]+", "");
            return reformatedString;

        } else
            return null;
    }

}
