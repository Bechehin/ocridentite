package com.esir.orange.ocridentity.ocrClasses;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.List;

/*
* Created by Oracion
*/

public class MyCamera extends SurfaceView implements SurfaceHolder.Callback {

    private SurfaceHolder mHolder;
    private Camera mCamera;
    private Camera.Parameters mParameters;
    private byte[] mBuffer;
    private boolean isFlashOff = true;
    private Camera.Size prPreviewSize;

    public MyCamera(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyCamera(Context context) {
        super(context);
        init();
    }

    public void init() {
        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public Bitmap getPic(int x, int y, int width, int height) {
        System.gc();
        Bitmap b = null;
        Camera.Size s = mParameters.getPreviewSize();

        YuvImage yuvimage = new YuvImage(mBuffer, ImageFormat.NV21, s.width, s.height, null);
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        yuvimage.compressToJpeg(new Rect(x, y, width, height), 100, outStream); // make JPG

        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inDither = true;
        opt.inPreferredConfig = Bitmap.Config.ARGB_8888;

        b = BitmapFactory.decodeByteArray(outStream.toByteArray(), 0, outStream.size()); // decode JPG
        yuvimage = null;
        outStream = null;
        System.gc();
        return b;
    }

    private void updateBufferSize() {
        try {
            mBuffer = null;
            System.gc();
            int h = mCamera.getParameters().getPreviewSize().height;
            int w = mCamera.getParameters().getPreviewSize().width;
            int bitsPerPixel = ImageFormat.getBitsPerPixel(mCamera.getParameters().getPreviewFormat());
            mBuffer = new byte[w * h * bitsPerPixel / 8];
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            mCamera = Camera.open();
        } catch (RuntimeException exception) {
            Toast.makeText(getContext(), "Camera broken, quitting !", Toast.LENGTH_LONG).show();
        }

        try {
            mCamera.setPreviewDisplay(holder);
            updateBufferSize();
            mCamera.addCallbackBuffer(mBuffer);
            mCamera.setPreviewCallbackWithBuffer(new Camera.PreviewCallback() {
                public synchronized void onPreviewFrame(byte[] data, Camera c) {

                    if (mCamera != null) {
                        mCamera.addCallbackBuffer(mBuffer);
                    }
                }
            });
        } catch (Exception exception) {
            if (mCamera != null) {
                mCamera.release();
                mCamera = null;
            }
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mCamera.stopPreview();
        mCamera.release();
        mCamera = null;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        try {
            mParameters = mCamera.getParameters();
            mParameters.set("orientation", "landscape");

            setCameraFocus();

           /* List<Camera.Size> sizes = mParameters.getSupportedPreviewSizes();

            // set parameters

            Camera.Size optSize = getOptimalPreviewSize(sizes, getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);

            mParameters.setPreviewSize(optSize.width, optSize.height);

            mCamera.setParameters(mParameters);*/

        } catch (Exception e) {
        }
        updateBufferSize();
        mCamera.startPreview();
    }


    public Camera.Parameters getCameraParameters() {
        return mCamera.getParameters();
    }

    public void setCameraFocus() {

        List<String> focusModes = mParameters.getSupportedFocusModes();

        if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            mParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        } else if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
            mParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        } else if (focusModes.contains(Camera.Parameters.FOCUS_MODE_MACRO)) {
            mParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_MACRO);
        } else if (focusModes.size() > 0) {
            mParameters.setFocusMode(focusModes.get(0));
        }

        mCamera.setParameters(mParameters);
    }

    public void zoomCamera() {
        int zoom = mParameters.getZoom();
        int maxZoom = mParameters.getMaxZoom();

        if (zoom == maxZoom) {
            zoom = 0;
        } else if (zoom == maxZoom / 2) {
            zoom = maxZoom;
        } else {
            zoom = maxZoom / 2;
        }
        mParameters.setZoom(zoom);
        mCamera.setParameters(mParameters);
    }

    public void setFlash() {
        String hasTorch = mParameters.getFlashMode();
        if (hasTorch != null) {
            if (!isFlashOff) {
                mParameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                isFlashOff = true;
            } else {
                mParameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                isFlashOff = false;
            }
            mCamera.setParameters(mParameters);
        } else {
            Toast.makeText(getContext(), "Your phone hasn't flash feature !", Toast.LENGTH_SHORT).show();
        }
    }

  /*  private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) h / w;

        if (sizes == null) return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }*/
}
