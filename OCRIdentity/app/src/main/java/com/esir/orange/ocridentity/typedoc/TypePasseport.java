package com.esir.orange.ocridentity.typedoc;

import android.util.Log;

import com.esir.orange.ocridentity.ocrClasses.ScanInformation;

/**
 * Created by Jean_jean on 2/2/2015.
 */
public class TypePasseport extends DocumentType {

    private String TAG = "Passeport decode ";
    ScanInformation information = new ScanInformation();

    public TypePasseport() {
        //set(width,height)
        boxHeight = 80;
        boxWidth = 450;

        type = "passeport";
    }

    //see https://en.wikipedia.org/wiki/Machine-readable_passport
    //TODO check a et b is incorrect
    @Override
    public boolean algorithm(String decode) {

      /*  boolean a = false;
        boolean b = false;
        boolean c = false;
        boolean d = false;*/

        try {
            information.setType(decode.substring(0, 1));
            information.setCodePays(decode.substring(2, 5));

            int endName = decode.indexOf("<<");
            information.setName(decode.substring(5, endName));

            //  a = controle(decode.substring(45, 53),Integer.parseInt(decode.substring(53, 54)));

            int endFirstName = decode.indexOf("<<<");
            String firstN = decode.substring(endName + 2, endFirstName);
            information.setFirstName(firstN.replace('<', ' '));

            int i = endFirstName;
            while (i < decode.length() && decode.charAt(i) == '<') {
                i++;
            }

            int endNum = i + 10;
            information.setDocumentNumber(decode.substring(i, endNum));
            int endBirth = endNum + 9;
            information.setBirthday(information.getTrueDate(decode.substring(endNum + 3, endBirth), 0));
            int endExp = endBirth + 8;
            information.setExpirationDate(information.getTrueDate(decode.substring(endBirth + 2, endExp), 0));
            //TODO check last digit

        } catch (Exception e) {
            Log.i(TAG, "exeption " + e.toString());
        }
        // return a & b & c & d;
        return true;
    }

    @Override
    public boolean controle(String decode, int cle) {
        int[] facteurs = {7, 3, 1};
        int resultat = 0;
        int valeur = 0;

        char[] chararray = decode.toCharArray();

        for (int i = 0; i < chararray.length; i++) {
            char c = chararray[i];
            if (c == '<') {
                valeur = 0;
            } else if (c >= 'A' && c <= 'Z') {
                int codeASCII = (int) c;
                valeur = codeASCII - 55;
            } else if (c >= '0' && c <= '9') {
                valeur = (int) c - 48;
            }
            resultat = valeur * facteurs[i % 3];
        }
        int cleDeControle = resultat % 10;
        return cle == cleDeControle;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public ScanInformation getScanInformation() {
        return information;
    }

}
