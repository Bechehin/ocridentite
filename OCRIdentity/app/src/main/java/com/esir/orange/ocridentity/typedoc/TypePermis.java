package com.esir.orange.ocridentity.typedoc;

import android.graphics.Point;

import com.esir.orange.ocridentity.ocrClasses.ScanInformation;

/**
 * Created by Jean_jean on 2/20/2015.
 */
public class TypePermis extends DocumentType {

    public TypePermis() {
        boxHeight = 80;
        boxWidth = 450;

        type = "permis";
    }

    @Override
    public boolean algorithm(String decode) throws IndexOutOfBoundsException, NumberFormatException {
        return false;
    }

    @Override
    public boolean controle(String decode, int cle) {
        return false;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public ScanInformation getScanInformation() {
        return null;
    }
}
