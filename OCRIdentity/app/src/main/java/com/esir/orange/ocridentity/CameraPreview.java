package com.esir.orange.ocridentity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.esir.orange.ocridentity.ocrClasses.MyCamera;
import com.esir.orange.ocridentity.typedoc.DocumentType;

/*
* Created by Oracion
*/

public class CameraPreview extends Activity {

    private MyCamera mPreview;
    private int mScreenHeight, mScreenWidth;
    private int boxHeight, boxWidth;
    private boolean overlay_display = true;
    private ImageButton btn_c, btn_f;
    public int left_x = 0, left_y = 0, right_x = 0, right_y = 0;
    private int x1, x2, y1, y2;
    ImageView imageOverlay;
    DocumentType s_type_doc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_preview);
        s_type_doc = (DocumentType) getIntent().getSerializableExtra("s_type_doc");

        if (getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // get phone resolution
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            mScreenHeight = displaymetrics.heightPixels;
            mScreenWidth = displaymetrics.widthPixels;

            //set box height and width depending on phone's resolution

            if (mScreenWidth < 961) {
                int offset = mScreenWidth - (97 * mScreenWidth) / 100;
                boxWidth = mScreenWidth - 2 * offset;
                if (mScreenWidth == 960) {
                    boxHeight = 110;
                } else if (mScreenWidth > 480) {
                    boxHeight = 90;
                } else if (mScreenWidth < 480) {
                    boxHeight = 50;
                }
            }

            boxWidth = (int)(mScreenWidth * 3 / 5);
            boxHeight = (int)(boxWidth * 7 / 10);

            boxWidth = s_type_doc.getBoxWidth(mScreenWidth, boxWidth);
            boxHeight = s_type_doc.getBoxHeight(mScreenWidth, boxHeight);

            calculateBoxCoordinates();

            mPreview = new MyCamera(this);

            FrameLayout frameLayout = (FrameLayout) findViewById(R.id.camera_preview_lt);
            frameLayout.addView(mPreview);

            LinearLayout overlay = (LinearLayout) findViewById(R.id.camera_overlay);
            overlay.bringToFront();

            overlay_display = s_type_doc.hasOverlay();
            if(overlay_display) {
                imageOverlay = (ImageView) overlay.findViewById(R.id.overlay);
                imageOverlay.setImageResource(s_type_doc.getOverlayResource());
                imageOverlay.getLayoutParams().height = y2 - y1;
                imageOverlay.getLayoutParams().width = x2 - x1;
                imageOverlay.requestLayout();
            }

            // begin capture button
            btn_c = (ImageButton) findViewById(R.id.btn_capture);

            btn_c.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Thread tGetPic = new Thread(new Runnable() {
                        public void run() {

                            Double[] ratio = getRatio();

                            if (!overlay_display) {
                                left_x = 0;
                                left_y = 0;
                                right_x = mScreenWidth;
                                right_y = mScreenHeight;
                            } else {
                                Rect imageBounds = new Rect();
                                imageOverlay.getGlobalVisibleRect(imageBounds);
                                left_x = imageBounds.left;
                                left_y = imageBounds.bottom + (26 * (imageBounds.top - imageBounds.bottom) / 100);
                                right_x = imageBounds.right;
                                right_y = imageBounds.bottom;
                            }

                            int left = (int) (ratio[1] * (double) left_x);

                            int top = (int) (ratio[0] * (double) left_y);

                            int right = (int) (ratio[1] * (double) right_x);

                            int bottom = (int) (ratio[0] * (double) right_y);

                            if (Image_preview.bitmap == null) {
                                Information_preview.capture = 1;
                                Image_preview.bitmap = mPreview.getPic(left, top, right, bottom);
                                Intent intent = new Intent(getApplicationContext(), Image_preview.class);
                                intent.putExtra("s_type_doc", s_type_doc);
                                startActivity(intent);
                            }
                            finish();
                        }
                    });
                    tGetPic.start();

                }
            });
            // end capture button

            // begin flash button
            btn_f = (ImageButton) findViewById(R.id.btn_Flash);

            btn_f.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPreview.setFlash();
                }
            });
            // end flash button


        } else {
            Toast.makeText(getApplicationContext(), "Your phone doesn't have a camera !", Toast.LENGTH_SHORT).show();
        }
    }


    public void calculateBoxCoordinates() {
        // calculate box coordinates on screen
        x1 = (mScreenWidth - boxWidth) / 2;
        int deltaWidth = x1 /2;
        x1 -= deltaWidth;

        y1 = (mScreenHeight - boxHeight) / 2;

        x2 = (mScreenWidth + boxWidth) / 2 - deltaWidth;

        y2 = (mScreenHeight + boxHeight) / 2;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    // get the ratio between the screen and camera preview size
    public Double[] getRatio() {
        Camera.Size s = mPreview.getCameraParameters().getPreviewSize();
        double heightRatio = (double) s.height / (double) mScreenHeight;
        double widthRatio = (double) s.width / (double) mScreenWidth;
        Double[] ratio = {heightRatio, widthRatio};
        return ratio;
    }
}
