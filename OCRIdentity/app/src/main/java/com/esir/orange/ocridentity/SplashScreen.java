package com.esir.orange.ocridentity;

import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.esir.orange.ocridentity.ocrClasses.FilesManagement;
import com.esir.orange.ocridentity.ocrClasses.OcrParameters;
import com.googlecode.tesseract.android.TessBaseAPI;


public class SplashScreen extends ActionBarActivity {

    private static int SPLASH_TIME_OUT = 2000;
    FilesManagement filesManagement = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        filesManagement = new FilesManagement();
        try {

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    //call the async Task
                    new InitialiseApplication().execute();
                }
            }, SPLASH_TIME_OUT);

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Error Initialize : ", e.getMessage());
            Toast.makeText(getApplicationContext(), "Error while initializing, application is closing...", Toast.LENGTH_SHORT).show();
            finish();
        }
    }


    private class InitialiseApplication extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {

            FilesManagement fm = new FilesManagement();

            if (fm.copyLanguageFiles(getApplicationContext())) {

                // load application preferences
                SettingsActivity.loadPreferences(getApplicationContext());

                // initialize tesseract OCR
                DocumentsList.baseAPI = new TessBaseAPI();
                DocumentsList.baseAPI.setDebug(true);

                DocumentsList.baseAPI.setVariable("textord_max_noise_size", "2");
                DocumentsList.baseAPI.setVariable("textord_heavy_nr ", "1");
                DocumentsList.baseAPI.setVariable("tessedit_ocr_engine_mode", "2"); // 0: Tesseract ; 1 : Cube ; 2:Both Tesseract and Cube engines
                DocumentsList.baseAPI.setVariable("tessedit_char_whitelist", "ABCDEFGHIJKLMNOPQRSTUVWXYZ<0123456789-");
                DocumentsList.baseAPI.setVariable("tessedit_char_balcklist", "%ù*µ$£=+})°]à@ç^_€\\è`-|(['{\"#é~&œ>«“");
                DocumentsList.baseAPI.setVariable("segment_penalty_dict_nonword", "10");
                DocumentsList.baseAPI.setVariable("segment_penalty_garbage", "10");
                // ocr_engine.setVariable("stopper_nondict_certainty_base", "-100");
                DocumentsList.baseAPI.setVariable("language_model_penalty_non_freq_dict_word", "10");
                DocumentsList.baseAPI.setVariable("language_model_penalty_non_dict_word", "10");
                DocumentsList.baseAPI.setPageSegMode(TessBaseAPI.PageSegMode.PSM_SINGLE_LINE);

                DocumentsList.baseAPI.init(filesManagement.getTESS_FILES_PATH(), OcrParameters.OCR_LANGUAGE);

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);

                startActivity(intent);

            } else {
                Toast.makeText(getApplicationContext(), "Error while initializing, check that a SD card is mounted !", Toast.LENGTH_SHORT).show();
            }

            finish();

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }

}
