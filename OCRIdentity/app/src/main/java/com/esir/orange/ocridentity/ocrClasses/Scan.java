package com.esir.orange.ocridentity.ocrClasses;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by Oracion on 12/12/2014.
 */
public class Scan implements Serializable{

    private String name;
    private Bitmap image;
    private String create_date;
    private String type;
    private int origin;
    private String path;
    private Boolean cloud = false;

    public Scan(String name, Bitmap image, String type, String create_date) {
        this.name = name;
        this.image = image;
        this.type = type;
        this.create_date = create_date;
    }

    public Scan() {
    }

    public int getOrigin() {
        return origin;
    }

    public void setOrigin(int origin) {
        this.origin = origin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public void setPath(String path){this.path = path;}
    
    public String getPath(){return path;}
}
