package com.esir.orange.ocridentity.ocrClasses;

import java.io.Serializable;

/**
 * Created by Oracion on 12/12/2014.
 */
public class ScanInformation implements Serializable {

    private String name;
    private String type;
    private String firstName;
    private String birthday;
    private String codePays;
    private String expirationDate;
    public String emissionDate;
    private String documentNumber;
    private String id;
    private String codeAdministration;
    private String sexe;

    public ScanInformation() {
    }


    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getCodeAdministration() {
        return codeAdministration;
    }

    public void setCodeAdministration(String codeAdministration) {
        this.codeAdministration = codeAdministration;
    }

    public String getID() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodePays() {
        return codePays;
    }

    public void setCodePays(String codePays) {
        this.codePays = codePays;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmissionDate() {
        return emissionDate;
    }

    public void setEmissionDate(String emissionDate) {
        this.emissionDate = emissionDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }


    public String getTrueDate(String dateT, int offset) {
        String dateR = "";
        try {

            dateR = dateT.substring(dateT.length() - 2, dateT.length());
            dateR = dateR + "/" + dateT.substring(dateT.length() - 4, dateT.length() - 2);
            dateR = dateR + "/" + dateT.substring(0, 2 + offset);

        } catch (Exception e) {
        }
        return dateR;
    }
}
