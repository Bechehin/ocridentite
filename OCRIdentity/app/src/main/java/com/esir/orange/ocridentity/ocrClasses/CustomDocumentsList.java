package com.esir.orange.ocridentity.ocrClasses;

import android.app.Activity;
import android.content.Context;
import android.media.ExifInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.esir.orange.ocridentity.R;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Oracion on 12/12/2014.
 */
public class CustomDocumentsList extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List<Scan> documents_list;
    private String TAG = "CustomDocList";

    public CustomDocumentsList(Activity act, List<Scan> docs) {
        activity = act;
        documents_list = docs;
    }

    public void updateListView(List<Scan> docs) {
        documents_list = docs;
        //Triggers the list update
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return documents_list.size();
    }

    @Override
    public Object getItem(int position) {
        return documents_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.document_item, null);

        TextView nameT = (TextView) convertView.findViewById(R.id.name_doc_item);
        TextView typeT = (TextView) convertView.findViewById(R.id.type_doc_item);
        TextView dateT = (TextView) convertView.findViewById(R.id.date_doc_item);
        ImageView imgT = (ImageView) convertView.findViewById(R.id.img_doc_item);
        ImageView imgok = (ImageView) convertView.findViewById(R.id.ic_ok);

        // getting scan data for the row
        Scan s = documents_list.get(position);

        nameT.setText(s.getName());
        typeT.setText(s.getType());

        // define each scan
        if (s.getType().equals("picture")) {
            imgT.setImageResource(R.drawable.gall);
            String path = s.getPath();
            dateT.setText(s.getCreate_date());

            ExifInterface exifInterface = null;
            try {
                exifInterface = new ExifInterface(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (exifInterface != null) {
                if (exifInterface.getAttribute(ExifInterface.TAG_MODEL) != null) {
                    Log.i(TAG, exifInterface.getAttribute(ExifInterface.TAG_MODEL));
                    if (exifInterface.getAttribute(ExifInterface.TAG_MODEL).equals("true")) {
                        imgok.setImageResource(R.drawable.ic_ok);  // file is completely on cloud
                    }
                } else {
                    // file isn't on cloud
                    imgok.setImageResource(R.drawable.ic_ko);
                }
            }
        } else // element is a folder
        {
            imgT.setImageResource(R.drawable.fold);

            if (s.getPath() != null) {
                File file = new File(s.getPath());
                if (file.isDirectory()) {
                    int nb = 0;
                    for (String s1 : file.list()) {
                        if (s1.indexOf(".txt") < 0) {
                            nb++;
                        }
                    }
                    dateT.setText(String.valueOf(nb) + " element(s)");
                }
            }
        }
        return convertView;
    }
}
