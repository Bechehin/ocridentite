package com.esir.orange.ocridentity;

import com.esir.orange.ocridentity.typedoc.TypeCNI;
import com.esir.orange.ocridentity.typedoc.TypePasseport;

/**
 * Created by Jean_jean on 2/2/2015.
 */

public class TestAlgo {

    @Test
    public void test_algoCNI(){
        TypeCNI typeCNI = new TypeCNI();
        boolean test = typeCNI.algorithme("IDFRAREUVRE<<<<<<<<<<<<<<<<<<<7821120905782001557CHRISTOPHE<<TH9006142M8");
        asserTrue(test);
    }

    @Test
    public void test_algoCNI(){
        TypePasseport typePasseport = new TypePasseport();
        boolean test = typePasseport.algorithme("P<FRASPECIMEN<<NATACHA<<<<<<<<<<<<<<<<<<<<<<51FR501150FRA7307122F1603062<<<<<<<<<<<<<<02");
        asserTrue(test);
    }

}
