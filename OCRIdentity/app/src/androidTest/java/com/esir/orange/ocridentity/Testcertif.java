package com.esir.orange.ocridentity.certification;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Signature;
import java.security.SignatureException;

import org.junit.Before;
import org.junit.Test;

public class Textcertif {

	Certifiaction certification;
	
	@Before
	public void before() throws NoSuchProviderException, NoSuchAlgorithmException{
		certification = new Certifiaction();
		certification.create_clef();
	}
	
	@Test
	public void test() throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, IOException {
		byte [] test = {0,1,0,0,1};
		certification.hash_picture(test);
		Signature signature = Signature.getInstance("SHA256withRSA");
		Path sigfos = Paths.get(System.getProperty("user.dir")+ File.separator + "sign");
		byte[] test2 = Files.readAllBytes(sigfos);
		MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        byte[] hash_picture = messageDigest.digest(test);
		signature.initVerify(certification.getPub());
		signature.update(hash_picture);
		assertTrue(signature.verify(test2));
	}
}
